# divis-inability

![](https://img.shields.io/badge/written%20in-PHP%2C%20C%2C%20Bash-blue)

A roundabout way of testing divisibility.

Uses PHP to generate a state machine in C, runtime compiled with a bash function.

The ideas behind this might be useful if you are in a bizarre environment with MBs of executable segment space, zero-cost branches, fixed numerators, and also denied access to any real division functionality. Which is to say, never.


## Download

- [⬇️ divis-inability-r00.tar.xz](dist-archive/divis-inability-r00.tar.xz) *(1.23 KiB)*
